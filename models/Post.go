package models

import (
	"time"
)

type Post struct {
	Id            int64        `json:"id"`
	Title         string       `json:"title"`
	Excerpt       string       `json:"-"`
	Content       string       `json:"content"`
	UserId        int64        `json:"-"`
	User         *SimpleUser   `json:"created_by"`
	Groups     []*Group        `json:"groups"                 pg:",many2many:post_groups,joinFK:Group"`
	Reposts    []*Reposts      `json:"reposts"`
	PostLikes  []*User         `json:"likes"                  pg:",many2many:post_likes"`
	Comments   []*Comment      `json:"comments"`
	Media      []*Media        `json:"media"                  pg:",many2many:post_media,joinFK:Media"`
	Meta          struct {
		CommentCount       int          `json:"comment_count"`
		UserHasCommented   bool         `json:"user_has_commented"`
		TotalShares        int          `json:"total_shares"`
		// UserHasShared      bool         `json:"user_has_shared"`
		UsersSharedCount   int          `json:"users_shared_count"`
		GroupsSharedCount  int          `json:"groups_shared_count"`
		Views              int          `json:"views"`
		UserHasSmiled      bool         `json:"user_has_smiled"`
		SmileCount         int          `json:"smile_count"`
		UsersVote          interface{}  `json:"users_vote"`
		UserDidOffer       bool         `json:"user_did_offer"`
	}                          `json:"meta"`
	ToFollowers   bool         `json:"to_followers"           sql:",notnull"`
	PostType      string       `json:"post_type"`
	Price         float64      `json:"price"`
	Status        string       `json:"status"`
	OgData        interface{}  `json:"og_data"`
	CreatedAt     time.Time    `json:"created_at"             sql:",null"`
	UpdatedAt     time.Time    `json:"updated_at"             sql:",null"`
	DeletedAt    *time.Time    `json:"deleted_at,omitempty"   sql:",null"`
}

type Reposts struct {
	UserId        int64        `json:"-"`
	User         *SimpleUser   `json:"created_by"             pg:",fk:User"`
	PostId        int64        `json:"-"`
	GroupId       int64        `json:"-"`
	Group        *Group        `json:"group"`
	CreatedAt     time.Time    `json:"created_at"`
}
