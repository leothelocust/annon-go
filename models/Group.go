package models

import (
	"time"
)

type SimpleGroup struct {
	Id            int64        `json:"id"`
	Title         string       `json:"title"`
	Assets        interface{}  `json:"assets"`
	MemberCount   int64        `json:"member_count"`	
}

type Group struct {
	Id            int64        `json:"id"`
	Title         string       `json:"title"`
	Description   string       `json:"description"`
	Color         string       `json:"color"`
	ExternalUrl   string       `json:"external_url"`
	ImageUrl      string       `json:"image_url"`
	Private       bool         `json:"private"`
	Latitude      int64        `json:"latitude"              sql:",type:'decimal(13,10)"`
	Longitude     int64        `json:"longitude"             sql:",type:'decimal(13,10)"`
	CreatedBy     int64        `json:"-"`
	User         *SimpleUser   `json:"created_by"            pg:",fk:CreatedBy"`
	CategoryId    int64        `json:"category_id"`
	// Category     *Group   .     `json:"category"`
	Wyf           bool         `json:"wyf"`
	RequestPublicDate time.Time `json:"request_public_date"`
	ParentId      int64        `json:"parent_id"`
	// Parent       *Group        `json:"parent"`
	Assets        interface{}  `json:"assets"`
	MemberCount   int64        `json:"member_count"`
	CreatedAt     time.Time    `json:"created_at"             sql:",null"`
	DeletedAt    *time.Time    `json:"deleted_at,omitempty"   sql:",null"`
}
