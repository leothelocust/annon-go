package models

import (
	"time"
)

type Comment struct {
	Id            int64        `json:"id"`
	Content       string       `json:"content"`
	UserId        int64        `json:"-"`
	User         *SimpleUser   `json:"created_by"`
	PostId        int64        `json:"-"`
	Post         *Post         `json:"-"`
	OriginatingGroupId int64   `json:"originating_group_id"`
	CreatedAt     time.Time    `json:"created_at"             sql:",null"`
	UpdatedAt     time.Time    `json:"updated_at"             sql:",null"`
	DeletedAt    *time.Time    `json:"deleted_at,omitempty"   sql:",null"`	
}
