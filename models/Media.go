package models

import (
	"time"
)

type Media struct {
	Id            int64        `json:"id"`
	TypeId        int8         `json:"-"`
	Type         *MediaTypes   `json:"type"`
	Uri           string       `json:"uri"`
	UserId        int64        `json:"-"`
	// User         *User         `json:"created_by"`
	CreatedAt     time.Time    `json:"created_at"             sql:",null"`
	UpdatedAt     time.Time    `json:"updated_at"             sql:",null"`
	DeletedAt    *time.Time    `json:"deleted_at,omitempty"   sql:",null"`	
}

type MediaTypes struct {
	Id            int8         `json:"-"`
	Type          string       `json:"type"`
}
