package models

import (
	"time"
)

type SimpleUser struct {
	TableName     struct{}     `json:"-"                      sql:"users,alias:user"`
	Id            int64        `json:"id"`
	Name          string       `json:"name"                   sql:"first_name,alias:name"`
	FirstName     string       `json:"-"`
	LastName      string       `json:"-"`
	Icon          string       `json:"icon"`
}

type User struct {
	Id            int64        `json:"id"`
	FirstName     string       `json:"first_name"`
	LastName      string       `json:"last_name"`
	Email         string       `json:"email"`
	Password      string       `json:"-"`
	Birthday      time.Time    `json:"birthday"`
	AgeHidden     bool         `json:"age_hidden"`
	Icon          string       `json:"icon"`
	Cover         string       `json:"cover"`
	Posts      []*Post         `json:"posts,omitempty"`
	CreatedAt     time.Time    `json:"created_at"             sql:",null"`
	UpdatedAt     time.Time    `json:"updated_at"             sql:",null"`
	DeletedAt    *time.Time    `json:"deleted_at,omitempty"   sql:",null"`
}
