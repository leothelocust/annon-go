package utils

import (
	"fmt"
	"time"
)

func StartTimer() (time.Time) {
	return time.Now()
}

func EndTimer(st time.Time) {
	fmt.Printf("Timer took %v\n", time.Now().Sub(st))
}

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}
