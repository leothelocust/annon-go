package routes

import (
	"github.com/go-pg/pg"

	Model "thit-go/models"
	Utils "thit-go/utils"

	"encoding/json"
	_"fmt"
	"net/http"
	_"time"
)

func UserFeed(w http.ResponseWriter, req *http.Request) {
	st := Utils.StartTimer()
	defer Utils.EndTimer(st)
	
	db := pg.Connect(&pg.Options{
		Addr: "localhost:5432",
		User: "test_user",
		Password: "secret",
		Database: "test_db",
	})

	// db.OnQueryProcessed(func(event *pg.QueryProcessedEvent) {
	// 	query, err := event.FormattedQuery(); Utils.CheckError(err)

	// 	fmt.Printf("%s %s", time.Since(event.StartTime), query)
	// })

	var posts = make([]Model.Post, 100)

	err := db.Model(&posts).
		Column("post.*", "groups_users.*", "User", "Groups", "Groups.User", "Comments", "Media", "Reposts", "Reposts.User", "Reposts.Group", "PostLikes").
		Where("post.deleted_at IS NULL").
		Join("LEFT JOIN groups_users on post_groups.group_id = groups_users.group_id").
		Join("LEFT JOIN post_groups on post_groups.post_id = post.id").
		Limit(2).
		Select()
	Utils.CheckError(err)

	for i, post := range posts {
		posts[i].Meta.CommentCount = len(post.Comments)
		posts[i].Meta.GroupsSharedCount = 0
		posts[i].Meta.UsersSharedCount = 0
		posts[i].Meta.SmileCount = len(post.PostLikes)
		posts[i].Meta.TotalShares = len(post.Reposts)
		posts[i].Meta.UserDidOffer = false
		posts[i].Meta.UserHasCommented = false
		posts[i].Meta.UserHasSmiled = false
		posts[i].Meta.UsersVote = nil
		posts[i].Meta.Views = 0
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(posts); Utils.CheckError(err)
}
