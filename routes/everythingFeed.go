package routes

import (
	"github.com/go-pg/pg"

	Model "thit-go/models"
	Utils "thit-go/utils"

	"encoding/json"
	_"fmt"
	"net/http"
	_"time"
)

func EverythingFeed(w http.ResponseWriter, req *http.Request) {
	st := Utils.StartTimer()
	defer Utils.EndTimer(st)
	
	db := pg.Connect(&pg.Options{
		Addr: "localhost:5432",
		User: "test_user",
		Password: "secret",
		Database: "test_db",
	})

	// db.OnQueryProcessed(func(event *pg.QueryProcessedEvent) {
	// 	query, err := event.FormattedQuery(); Utils.CheckError(err)
	
	// 	fmt.Printf("%s %s", time.Since(event.StartTime), query)
	// })

	var posts = make([]Model.Post, 100)

	err := db.Model(&posts).
		Column("post.*", "User", "Groups", "Groups.User", "Comments.User", "Media", "Reposts", "Reposts.User", "Reposts.Group", "PostLikes").
		Where("post.deleted_at IS NULL").
		Limit(100).
		Select()
	Utils.CheckError(err)


	for i, post := range posts {
		posts[i].User.Name = posts[i].User.FirstName + " " + posts[i].User.LastName
		posts[i].Meta.CommentCount = len(post.Comments)
		posts[i].Meta.GroupsSharedCount = 0
		posts[i].Meta.UsersSharedCount = 0
		posts[i].Meta.SmileCount = len(post.PostLikes)
		posts[i].Meta.TotalShares = len(post.Reposts)
		posts[i].Meta.UserDidOffer = false
		posts[i].Meta.UserHasCommented = false
		posts[i].Meta.UserHasSmiled = false
		posts[i].Meta.UsersVote = nil
		posts[i].Meta.Views = 0
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(posts); Utils.CheckError(err)
}
