package main

import (
	Route "thit-go/routes"
	
	"net/http"
)

func main() {
	http.HandleFunc("/everything", Route.EverythingFeed)
	http.HandleFunc("/feed", Route.UserFeed)
	http.ListenAndServe(":5000", nil)
}
