# Installation

    git clone [this things]

    cd [this things]

    cd models/

    go build .; go install .

    cd routes/

    go build .; go install .

    cd utils/

    go build .; go install .


# Requirements

* Must have `postgres` running locally and accessable at `localhost:5432`.
* Must have `gulp` installed if wanting to `go install .` while editing.


# Startup

    go run main.go
