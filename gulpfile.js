var gulp = require('gulp');
var path = require('path');
var shell = require('gulp-shell');
var gutil = require('gulp-util');

var goPath = ['thit-go/models/*.go', 'thit-go/routes/*.go', 'thit-go/utils/*.go'];
var goWatchPath = ['models/*.go', 'routes/*.go', 'utils/*.go'];

gulp.task('install', function(){
    // return shell(['cd models; go install .; cd -', 'cd routes; go install .; cd -', 'cd utils; go install .; cd -']);
    return gulp.src(goPath, {read: false})
        .pipe(shell(['go install <%= stripPath(file.path) %>'],
                    {
                        templateData: {
                            stripPath: function(filepath) {
                                var subPath = filepath.substring(process.cwd().length+5);
                                var pkg = subPath.substring(0, subPath.lastIndexOf(path.sep));
                                return pkg;
                            }
                        }
                    })
             );
});

gulp.task('watch', function(){
    gulp.watch(goWatchPath, ['install']);
});
